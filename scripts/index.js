var searchedLang;
var currentJson;
var storedJson;
var app = angular.module('gitmodule',['ngRoute']);
  app.config(function($routeProvider){
    $routeProvider
      .when('/',{
          template:'<div>Home</div>',
      })
      .when('/search',{
        // template:'<p>data recieved</p>',
        templateUrl:'mainblock.html',
        controller:'searchController',
        controllerAs:'searchCtrl'
      })
      .otherwise({
        template:'<div>Wrong Page</div>'
      });
  });
  app.controller('initialize',function($location,$scope) {
    console.log("initialize");
    this.resTotal = 00;
    this.resLanguage = "Lang";
    this.searchData = function() {
      var lang = this.searchtext;
      searchedLang =lang;
      console.log("language:"+lang);
      $location.path('/search');
      console.log("after location");
    }

    this.seekChange = function() {
      var seekVal = this.seekbar;
      console.log(seekVal);
      var count2 = 0;
      currentJson = [];
      for(var i=0;i<currentJson.length;i++){

        if(parseInt(currentJson[i].stargazers_count) >= parseInt(seekVal)){
          currentJson.push(currentJson[i]);
          count2++;
        }
      }
      this.resTotal = count2;
    }
  });

  app.controller('searchController',function($scope,$http) {
    var thisRef = this;
    var lang = searchedLang;
    console.log("controller"+lang);
    var gitUrl = "https://api.github.com/search/repositories?q="+lang+"&page=1&per_page=100";
    var currentStart =0;
    $http({method:'GET',url:gitUrl})
      .success(function(data, status, headers, config) {
        if(data.items.length>0){
          console.log("data recieved:"+data.items.length);
          storedJson = [];
          storedJson.push(data.items);
          console.log(storedJson.length);
          currentJson = data.items;
          thisRef.rawjson = [];
          for(var i=0;i<10 && i<data.items.length;i++){
            thisRef.rawjson.push(currentJson[i]);
          }
          currentStart = i;
          console.log(currentStart+":"+i);
          $scope.x.resTotal = data.items.length;
        }
        else {
          thisRef.rawjson={};
          thisRef.rawjson.name = "No Match Found !";
        }
      })
      .error(function(data, status, headers, config) {
        console.log("error in getting data from github");
      });
    this.pageFirst = function() {
        console.log("first");
        thisRef.rawjson = [];
        for(var i=0;i<10 && i<currentJson.length;i++){
          thisRef.rawjson.push(currentJson[i]);
        }
        currentStart = i;
    }
    this.pagePrev = function() {
        console.log("prev");
        console.log(currentStart);
        if(currentStart > 10){
          thisRef.rawjson = [];
          var i = (currentStart%10) == 0 ? currentStart-20 : currentStart-10-(currentStart%10);

          for(i;i<currentStart-10 && i<currentJson.length;i++){
            thisRef.rawjson.push(currentJson[i]);
          }
          console.log(i);
          currentStart = i;
        }
    }
    this.pageNext = function() {
      if(currentStart < currentJson.length)
        thisRef.rawjson = [];
      console.log(thisRef.rawjson.length+":"+currentStart);
      for(var i=currentStart;i<currentStart+10 && i<currentJson.length;i++){
        thisRef.rawjson.push(currentJson[i]);
      }
      console.log(thisRef.rawjson.length);
      currentStart = i;
    }
    this.pageLast = function() {
      thisRef.rawjson = [];
      for(var i=currentJson.length-10;i<currentJson.length;i++){
        thisRef.rawjson.push(currentJson[i]);
      }
      currentStart = i;
    }
  });
