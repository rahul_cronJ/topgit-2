var app = angular.module("gitmodule",[]);

app.controller("maindata",["$scope",function($scope) {
  $scope.rawjson = gitdata.items;
  var max=0,min=5555555;
  $scope.resTotal = 30;
  $scope.resLanguage = "all";
  $scope.searchData = function() {
    var lang = $scope.searchtext;
    $scope.rawjson=[];
    var count = 0;
    for(var i=0;i<gitdata.items.length;i++){
      // gitdata.items[i].name.toLowerCase().includes(lang)||
      if(gitdata.items[i].language.toLowerCase().includes(lang)){
        $scope.rawjson.push(gitdata.items[i]);
        count++;
      }
    }
    console.log(count);
    $scope.resTotal = count;
    $scope.resLanguage = lang+"...";
  }
  $scope.seekChange = function() {
    var seekVal = $scope.seekbar;
    $scope.rawjson=[];
    var count2 = 0;
    for(var i=0;i<gitdata.items.length;i++){
      if(gitdata.items[i].stargazers_count >= parseInt(seekVal)){
        $scope.rawjson.push(gitdata.items[i]);
        count2++;
      }
    }
    $scope.resTotal = count2;
  }

  $scope.goToGithub = function(x) {
    window.location = x.html_url;
  }

  // console.log(rawjson[0].description);
}]);
